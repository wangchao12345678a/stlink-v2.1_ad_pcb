# StlinkV2.1_AD_PCB

#### 介绍
根据网上大佬的原理图绘制的Stlink板子

#### 原理图

![原理图](Picture/原理图.jpg)

#### PCB

![PCB](Picture/PCB.PNG)

![PCB_3D](Picture/PCB_3D.PNG)

#### 实物图

![实物图](Picture/实物图.jpg)